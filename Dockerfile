FROM node:18.18-alpine as build
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build

## run stage ##
FROM nginx:alpine
COPY --from=build /app/build /run
COPY nginx.conf /etc/nginx/nginx.conf